#ifndef _KEY_H_
#define _KEY_H_

#include "stm32f10x.h"

typedef enum
{
	key1,
	key1_press,
	key2,
	key2_press,
	key3,
	key3_press,
	key_notclick
}KEY;


void Key_GPIO_Config(void);
KEY KeyScan(void);




#endif

