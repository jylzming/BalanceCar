#ifndef _DATAPROCESS_H_
#define _DATAPROCESS_H_

#include "stm32f10x.h"
#include "moto.h"
#include "GYRO.h"


typedef struct 
{
	float SetPoint;	//Desired Value
	float err;		//实际值与设定值的差值
	float err_last; //上一次的实际值与设定值的差值
	float integral; //误差之和
	float sum;
}pid_t;






void Exchang_Data(short *accel,short *gyro,float *accconvert,float *gyroconvert);

float Pitch_Out(float *accData,float *gyroData, float pitch);

void Kalman_Filter(short *accel,short *gyro, double  Angle, int16_t Gyro_Y);   
float GetPitch(float *pRealVals) ;
float Balance_Control(float pitch1,short gyroDataY);
float Speed_Control(void);
void PWM_Control(int16_t pwm);
void PWM_Cntrl(int pwm);


float BalancePidCalc(float pitchdata);
float SpeedPidCalc(void);


#endif
