#ifndef _ENCODER_H
#define _ENCODER_H

#include "stm32f10x.h"

void TIM4_L_encoder_Config(void);
void TIM2_R_encoder_Config(void);

#endif //_ENCODER_H
