#ifndef __CONTROL_H_
#define __CONTROL_H_
#include "stm32f10x.h"
  /**************************************************************************
作者：平衡小车之家
我的淘宝小店：http://shop114407458.taobao.com/
**************************************************************************/
#define PI 3.14159265
#define ZERO_PIONT 1.5
extern	int Balance_PWM,Velocity_Pwm,Turn_PWM;
int EXTI15_10_IRQHandler(void);
int Balance(float angle,float gyro);
int Speed(int encoder_left,int encoder_right);
int turn(int encoder_left,int encoder_right,float gyro);
void Set_Pwm(int moto1,int moto2);
void Key(void);
void Xianfu_Pwm(void);
u8 Turn_Off(float angle, int voltage);
void Get_Angle(u8 way);
int myabs(int a);
int Pick_Up(float Acceleration,float Angle,int encoder_left,int encoder_right);
int Put_Down(float Angle,int encoder_left,int encoder_right);
#endif
