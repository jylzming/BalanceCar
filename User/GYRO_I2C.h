#ifndef _I2C_H_
#define _I2C_H_

#include "stm32f10x.h"

#define GYRO_ADDRESS    0xD0

void GYRO_I2C_Config(void);
void I2C_ByteWrite(u8 pbuf, u8 writeAddr);
void I2C_Read(u8 *pbuf, u8 readAddr, u16 num);
void I2C_WaitStandbyState(void);
void I2C_ClearBusy(void);


#endif
