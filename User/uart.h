#ifndef _UART_H_
#define _UART_H_

#include "stm32f10x.h"
#include "stdio.h"

extern char recv_data[255];
extern uint8_t recv_cnt;

void UART1_Config(uint32_t baud);
void UART3_Config(uint32_t baud);
void USART_SendStr(USART_TypeDef* USARTx, char *str);
void USART_SendRecvData(USART_TypeDef* USARTx);

#endif
