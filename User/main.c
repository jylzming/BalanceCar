/**********************************************************************************
 * 文件名  ：main.c
 * 描述    ：         
 * 库版本  ：ST3.5.0
 * 作者    ：牧梦苍穹  
 * 博客    ：http://jylzming.blog.163.com/
**********************************************************************************/

#include "stm32f10x.h"
#include "led.h"
#include "OLED_I2C.h"
#include "GYRO.h"
#include "uart.h"
#include "key.h"
#include "systick.h"
#include "moto.h"
#include "dataprocess.h"
#include "stdio.h"
#include "bluetooth.h"
#include "voltage.h"
#include "control.h"


short Accel[3];//加速度原始数据
short Gyro[3];//陀螺仪原始数据

float AccConvert[3];//加速度转换数据
float GyrConvert[3];//陀螺仪转换数据
float pitch;//俯仰角
int Balance_PWM,Speed_PWM,Turn_PWM;
int   MotoL_PWM,MotoR_PWM;
int   encoderL, encoderR; //左右编码器的脉冲计数
short Temperature;//温度
int   Voltage;//电池电压
int turn_flag = 0;//转向标志，-1向左，1向右
int qianhou_flag = 0;//前后标志，1向前，-1向后


extern pid_t blcpid;
extern pid_t spdpid;


void OLED_Show(void);




void TIM3_Config(void)
{
	TIM_TimeBaseInitTypeDef  TIM3_InitConfig;
	NVIC_InitTypeDef         NVIC_InitConfig;
	/*开时钟*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,  ENABLE);
	/*中断配置*/
	NVIC_InitConfig.NVIC_IRQChannel            = TIM3_IRQn;
	NVIC_PriorityGroupConfig  (NVIC_PriorityGroup_2) ;
	NVIC_InitConfig.NVIC_IRQChannelSubPriority = 0;//抢占优先级
	NVIC_InitConfig.NVIC_IRQChannelSubPriority = 0;//响应优先级
	NVIC_InitConfig.NVIC_IRQChannelCmd         = ENABLE;
	NVIC_Init(&NVIC_InitConfig);
	/*定时器6时钟配置*/
	TIM3_InitConfig.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM3_InitConfig.TIM_CounterMode   = TIM_CounterMode_Up;
	//5ms定时
	TIM3_InitConfig.TIM_Period        = 4999;
	TIM3_InitConfig.TIM_Prescaler     = 71;
	TIM_TimeBaseInit(TIM3, &TIM3_InitConfig);
	/*使能定时器溢出中断*/
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	/*使能定时器6*/
	TIM_Cmd(TIM3, ENABLE);
}



int main(void)
{	
	// 程序来到main函数之前，启动文件：statup_stm32f10x_hd.s已经调用
	//SystemInit();//函数把系统时钟初始化成72MHZ
	// SystemInit()在system_stm32f10x.c中定义
	// 如果用户想修改系统时钟，可自行编写程序修改
	SysTick_Init();//延时函数初始化
	UART1_Config(9600);//串口1函数初始化,与电脑通讯，调试用，调试完毕后清除中断的调试函数
	UART3_Config(9600);//与Bluetooth模块通讯
	printf("MPU6050\r\n");//电脑终端打印消息
	ADC_GPIO_Config();//ADC测量电压
	LED_GPIO_Config();//LED
	Key_GPIO_Config();//按键
	MotoPWM();//马达
	EncoderConfig();//马达编码器
	
	I2C_Configuration();//OLED IIC
	OLED_Init();
	OLED_CLS();
	OLED_ShowStr(19, 0, (char *)"Hello BalanceCar", 1);
//	OLED_ShowStr(0, 1, "BlcPWM:", 1);
//	OLED_ShowStr(0, 3, "SpdPWM:", 1);
//	OLED_ShowStr(0, 5, "Pitch:", 1);
//	OLED_ShowStr(0, 7, "Vbat:", 1);
//	OLED_ShowStr(66, 7, "PWM:", 1);
	MPU6050_Init();//陀螺仪初始化
	MPU6050ReadID();
	
	
	if(SendCmd("AT\r\n", "OK", 500) == TRUE)
	{
		SendCmd("AT+RESET\r\n", 0, 0);
	}
	
	
	TIM3_Config();//定时器任务
	OLED_CLS();
	while( 1 )
	{
		OLED_Show();
	}
}







/**************************************************************************

**************************************************************************/
void TIM3_IRQHandler(void)
{
	
	if(TIM_GetITStatus(TIM3 , TIM_IT_Update) == SET)
	{
		
		Get_Angle(3); //获取俯仰角等
		Voltage = VoltageConvert(); //获取电池电压
		
		Balance_PWM  = Balance(pitch, (-Gyro[1]));  //平衡PID控制
		Speed_PWM = Speed(encoderL, encoderR); //速度环PID控制	
		Turn_PWM  = turn(encoderL, encoderR, Gyro[2]); //转向环PID控制
		MotoL_PWM = Balance_PWM + Speed_PWM - Turn_PWM; //计算左轮电机最终PWM
		MotoR_PWM = Balance_PWM + Speed_PWM + Turn_PWM; //计算右轮电机最终PWM
		Set_Pwm(MotoL_PWM, MotoR_PWM);//控制马达

		TIM_ClearITPendingBit(TIM3 , TIM_IT_Update);
	}
}	

//void TIM3_IRQHandler(void)
//{
//	//static uint8_t Flag_Target = 1;
//	if(TIM_GetITStatus(TIM3 , TIM_IT_Update) == SET)
//	{
//		//读取电池电压
//		Voltage = VoltageConvert();
//		//读取陀螺仪原始数据
//		MPU6050ReadAcc(Accel);	
//		MPU6050ReadGyro(Gyro);		
//		//原始数据转化为角度加速度信息
//		Exchang_Data(Accel, Gyro, AccConvert, GyrConvert);
//		
//		
//		//通过一阶互补滤波解算出俯仰角
//		pitch = Pitch_Out(AccConvert, GyrConvert, pitch);
//		//读取陀螺仪数据
//		//Banlace_PWM=Balance_Control(pitch,Gyro[1]);//平衡PID控制	
//		Banlace_PWM = BalancePidCalc(pitch);//平衡PID控制	
//		
//		//KP*encoder+KI*encoder_integral
////			Speed_PWM=Speed_Control();//速度环PID控制
//		Speed_PWM = SpeedPidCalc();
//		
//		//PWM_Control((int16_t)(Banlace_PWM+Speed_PWM));//平衡控制+速度控制
//		Ctrl_PWM = (int)(Banlace_PWM + Speed_PWM);
//		PWM_Cntrl(Ctrl_PWM);


//		TIM_ClearITPendingBit(TIM3 , TIM_IT_Update);
//	}
//}	

void OLED_Show(void)
{
	char buff[32] = {0};	
	OLED_ShowStr(0, 1, "BlcPWM:", 1);
	OLED_ShowStr(0, 3, "SpdPWM:", 1);
	OLED_ShowStr(0, 5, "Pitch:", 1);
	OLED_ShowStr(0, 7, "Vbat:", 1);
	OLED_ShowStr(66, 7, "PWM:", 1);

	OLED_ShowFloatNum(30, 7, Voltage, 1, 1);
	OLED_ShowFloatNum(42, 5, pitch, 2, 1);
	OLED_ShowNum(48, 1, Balance_PWM, 1);
	OLED_ShowNum(48, 3, Speed_PWM, 1);
	

	if(MotoL_PWM < -7200)
	{
		OLED_ShowNum(96, 7, -100, 1);
	}
	else if(MotoL_PWM > 7200)
	{
		OLED_ShowNum(96, 7, 100, 1);
	}
	else
	{
		OLED_ShowNum(96, 7, (MotoL_PWM / 72), 1);
	}	
}
