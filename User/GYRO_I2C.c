#include "GYRO_I2C.h"

/**************************************************************
*函数名：I2C_Config
*参  数：无
*返回值：无
*概  述：配置I2C1，第一步打开相关外设时钟，第二步填充结构体，第三步配置到I2C1并使能
**************************************************************/
void GYRO_I2C_Config(void)
{
	I2C_InitTypeDef I2C_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//开启时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	
	GPIO_PinRemapConfig(GPIO_Remap_I2C1, ENABLE);
	
	/*硬件I2C: PB6(重映射PB8) -- SCL; PB7(重映射PB9) -- SDA */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = 400000;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_OwnAddress1 = 0x30;//只要跟外设地址不同即可
	
	I2C_Init(I2C1, &I2C_InitStructure);	
	
	I2C_Cmd(I2C1, ENABLE);
}


/**************************************************************
*函数名：I2C_ByteWrite
*参  数：writeAddr待写入数据的地址，pbuf待写入的数据
*返回值：无
*概  述：向外设发送一个字节数据，每做一个步骤都要判断相应的操作是否正常完成
**************************************************************/
void I2C_ByteWrite(u8 pbuf, u8 writeAddr)
{
	//I2C_WaitStandbyState();
	volatile uint32_t tmp = 0;
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) == SET)
	{
		tmp += 1;
		if(tmp >= 65536)
		{
			tmp = 0;
			I2C_ClearBusy();
			break;
		}
	}
	
	I2C_GenerateSTART(I2C1, ENABLE);//发送起始信号
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) == ERROR);
	
	I2C_Send7bitAddress(I2C1, GYRO_ADDRESS, I2C_Direction_Transmitter);//0x68
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) == ERROR);
	
	I2C_SendData(I2C1, writeAddr);
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) == ERROR);
	
	I2C_SendData(I2C1, pbuf);
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) == ERROR);
	
	I2C_GenerateSTOP(I2C1, ENABLE);
}




/**************************************************************
*函数名：I2C_Read
*参  数：readAddr待读取数据的地址，pbuf待写入的数据，num读取字节数
*返回值：无
*概  述：从外设中读取num个字节数据，每做一个步骤都要判断相应的操作是否正常完成
**************************************************************/
void I2C_Read(u8 *pbuf, u8 readAddr, u16 num)
{
	//I2C_WaitStandbyState();
	volatile uint32_t tmp = 0;
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) == SET)
	{
		tmp += 1;
		if(tmp >= 65536)
		{
			tmp = 0;
			I2C_ClearBusy();
			break;
		}
	}
	
	I2C_GenerateSTART(I2C1, ENABLE);//发送起始信号
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) == ERROR);
	
	I2C_Send7bitAddress(I2C1, GYRO_ADDRESS, I2C_Direction_Transmitter);//0x68
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) == ERROR);
	
	I2C_SendData(I2C1, readAddr);
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) == ERROR);

	I2C_GenerateSTART(I2C1, ENABLE);
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) == ERROR);
	
	I2C_Send7bitAddress(I2C1, GYRO_ADDRESS, I2C_Direction_Receiver);//0x68
	while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) == ERROR);
	
	while(num)
	{
		if(num == 1)
		{
			I2C_AcknowledgeConfig(I2C1, ENABLE);
			
			I2C_GenerateSTOP(I2C1, ENABLE);
		}
		if(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED) == SUCCESS)
		{
			*pbuf = I2C_ReceiveData(I2C1);
			pbuf++;
			num--;
		}
		
	}
	I2C_AcknowledgeConfig(I2C1, ENABLE);
}

/**************************************************************
*函数名：I2C_WaitStandbyState
*参  数：无
*返回值：无
*概  述：等待I2C稳定
**************************************************************/
void I2C_WaitStandbyState(void)
{
	vu16 SR1_tmp = 0;
	
	do
	{
		I2C_GenerateSTART(I2C1, ENABLE);
		
		SR1_tmp = I2C_ReadRegister(I2C1, I2C_Register_SR1);
		
		I2C_Send7bitAddress(I2C1, GYRO_ADDRESS, I2C_Direction_Transmitter);//0x68
	}
	while((I2C_ReadRegister(I2C1, I2C_Register_SR1) & 0x0002) == 0);
	
	I2C_ClearFlag(I2C1, I2C_FLAG_AF);
	I2C_GenerateSTOP(I2C1, ENABLE);
}


void I2C_ClearBusy(void)
{
	I2C1->CR1 |= 0x8000;
	I2C1->CR1 &= ~0x8000;
}


