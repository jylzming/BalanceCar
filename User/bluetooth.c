#include "bluetooth.h"
#include "uart.h"
#include "systick.h"
#include <string.h>
//#include <ctype.h>
//#include <stdio.h>
//#include <stdint.h>

/************************************************************************************
*函数名：ClearBuf
*参  数：char *s
*返回值：无
*描  述：清除指针s指向的数组或字符串
************************************************************************************/
void ClearBuf(char *s)
{
	uint16_t i,len;
	len = strlen(s);
	for(i = 0; i < len; i++)
	{
		*s = '\0';
	}
}


/************************************************************************************
*函数名：SendCmd
*参  数：char *cmd 			待发送的命令字符串
*参  数：char *reply 		模块返回的指令信息 
*参  数：uint32_t waittime	等待时间
*返回值：Bitstat   命令发送回复正常返回TRUE，错误返回FALSE
*描  述：发送AT指令
************************************************************************************/
Bitstat SendCmd(char *cmd, char *reply, uint32_t waittime)
{
	volatile uint8_t i = 0;
	char *s = NULL;
	for(i = 0; i < 255; i++)
	{
		recv_data[i] = 0;
	}
	byte_cnt = 0;//recv_data[byte_cnt]从数组位置0开始保存
	USART_SendStr(USART3, cmd);
	if(reply == 0)
	{
		//USART_Cmd(USART3, DISABLE);//关闭串口接收返回数据
		Delay_ms(waittime);
		//USART_Cmd(USART3, ENABLE);//记得要打开
		return TRUE;
	}
	Delay_ms(waittime);
	
	//获取返回字符串，并进行判断是否正确
	s = recv_data;	
	if(strstr(s, reply) == NULL)//若返回数据中不含响应数据
	{
		return FALSE;
	}
	else//指令正确返回TRUE
	{
		return TRUE;
	}	
}


