#include "systick.h"

static __IO u32 TimingDelay;

/*SysTick时钟源由HCLK/8提供，当系统频率为72MHz时最小计数周期为1/9MHz,
计满9次为1us，fac_us以9为单位*/
void SysTick_Init(void)
{
	if (SysTick_Config(72))	
	{ 
		/* Capture error */ 
		while (1);
	}
	//NVIC_SetPriority(SysTick_IRQn, 0);延时优先级最好不要设置高，否则一般函数延时导致其它中断进不来
	SysTick->CTRL &= ~ SysTick_CTRL_ENABLE_Msk;	
}


void Delay_us(u32 us)
{
	TimingDelay = us;
	// 使能滴答定时器  
	SysTick->CTRL |=  SysTick_CTRL_ENABLE_Msk;
	while(TimingDelay != 0);
}

void Delay_ms(u32 ms)
{
	while(ms--)
	{
		Delay_us(1000);
	}
}

void TimingDelay_Decrement(void)
{
	if (TimingDelay != 0x00)
	{ 
		TimingDelay--;
	}
}



