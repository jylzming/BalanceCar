#ifndef _GPRS_H_
#define _GPRS_H_

#include "stm32f10x.h"

extern char recv_data[255];
extern uint8_t recv_cnt;
extern uint8_t byte_cnt;
extern uint8_t framedata_flag;


typedef enum 
{
    GPRS_STATE_AT_CHECK,
    GPRS_STATE_AT_CREG,
    GPRS_STATE_AT_CGDCONT,
    GPRS_STATE_AT_CGQMIN,
    GPRS_STATE_AT_CGQREQ,
    GPRS_STATE_AT_SCFG,
    GPRS_STATE_AT_SGACT_GET,
    GPRS_STATE_AT_SGACT_SET,
    GPRS_STATE_AT_SD,
    GPRS_STATE_AT_SH
} GPRS_STATES;

typedef enum
{
	TRUE,
	FALSE
}Bitstat;


Bitstat SendCmd(char *cmd, char *reply, uint32_t waittime);
Bitstat GPRS_DefaultConnect(void);
Bitstat GPRS_Connect(char *ip, uint32_t webport, uint32_t localport, uint8_t socketID,uint8_t repeat);
Bitstat Creg(void);
	
void GSM_WaitForNetworks(void);
char * HttpPostPkg(char *apikey, char *devid, char *data);
char * HttpGetPkg(char *apikey, char *devid, char *strid);



#endif

