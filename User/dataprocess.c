#include "dataprocess.h"
#include "math.h"
#include "moto.h"


#define PI 3.14159265

float encodernow,encoder;//编码器值和上一次的值
float encoder_integral;
extern short Accel[3];//加速度原始数据
extern short Gyro[3];//陀螺仪原始数据
extern float AccConvert[3];//加速度转换数据
extern float GyrConvert[3];//陀螺仪转换数据
extern int Voltage;//电池电压
extern float pitch;//俯仰角

pid_t blcpid;
pid_t spdpid;


float Angle_Balance,Gyro_Balance,Gyro_Turn; //平衡倾角 平衡陀螺仪 转向陀螺仪
float angle;

/**************************************************************************
函数功能：异常关闭电机
入口参数：倾角和电压
返回  值：1：异常  0：正常
**************************************************************************/
void Turn_Off(float angle, int voltage)
{
	if(angle<-40||angle>40||voltage<11/*||1==Flag_Stop*/)//电池电压低于11.1V关闭电机
		MotoStop();
}


//pid struct data init
pid_t pid_init(float SetPoint, float err, float err_last,float integral, float sum)
{
	pid_t * tset;

	tset->SetPoint 	= SetPoint;
	tset->err 		= err;
	tset->err_last 	= err_last;
	tset->integral 	= integral;
	tset->sum		= sum;

	return *tset;
}


void Exchang_Data(short *accel,short *gyro,float *accconvert,float *gyroconvert)
{
	uint8_t i;
	for(i=0;i<3;i++)
	{
		accconvert[i] = accel[i] / 16384.0f;//a = 2g*(ACC/32768)其中g为重力加速度，ACC为陀螺仪获取的数据
		gyroconvert[i] = gyro[i] / 16.384f;//MPU6050数据是16位的数，满量程-+2000，即4000. 65536/4000=16.384
	}
	//gyroconvert[1] -= 12.2f;//这是为什么呢？重心不在中点测定的偏移？
}


float Pitch_Out(float *accData,float *gyroData, float pitch)
{
	float tmp;
	//atan2(Accel_X,Accel_Z)*180/PI;计算与地面的夹角
	tmp = atan2(accData[0], accData[2]) * 180.0 / PI;
	
	//一阶低通滤波的算法公式为：Y(n)=αX(n) +(1-α)Y(n-1)
	//式中：α=滤波系数；X(n)=本次采样值；Y(n-1)=上次滤波输出值；Y(n)=本次滤波输出值。
	//一阶低通滤波法采用本次采样值与上次滤波输出值进行加权，得到有效滤波值，使得输出对输入有反馈作用。
	pitch = 0.02f * tmp + (1 - 0.02) * (pitch - gyroData[1] * 0.005);
	return pitch;
}


float BalancePidCalc(float pitchdata)
{
	float Kp = 100;
	float Ki = 0 ;
	float Kd = 0.4;
	static pid_t pid;
	pid.SetPoint  = 3;//平衡点角度

	pid.err = pitchdata - pid.SetPoint;//当前偏差
	pid.integral += pid.err;//偏差之和	
	
    //Uo = Kp*本次误差 + Ki*所有误差之和 + Kd*误差变化率
    pid.sum = Kp * pid.err + Ki * pid.integral + Kd * (-GyrConvert[1]);//(pid.err - pid.err_last);	//正+正+负
	pid.err_last = pid.err;//计算完毕后把本次偏差值赋给上次偏差值
	
	return pid.sum;
}


float SpeedPidCalc(void)
{
	float Kp = 50;
	float Ki = Kp / 200;
	//float Kd = 0;	
	static pid_t pid;
	int encoderL, encoderR;


//	static float Velocity,Encoder, Encoder_last,Movement;
//	static float Encoder_Integral,Target_Velocity=130;
//	float kp=50,ki=kp/200;
//	//=============速度PI控制器=======================//
//	Encoder = (Encoder_Left+Encoder_Right) - 0;                    //===获取最新速度偏差==测量速度（左右编码器之和）-目标速度（此处为零）
//	Encoder_last *= 0.7;		                                                //===一阶低通滤波器
//	Encoder_last += Encoder * 0.3;	                                    //===一阶低通滤波器
//	Encoder_Integral += Encoder_last;                                       //===积分出位移 积分时间：10ms
//	Encoder_Integral = Encoder_Integral - Movement;                       //===接收遥控器数据，控制前进后退
//	
//	if(Encoder_Integral>15000)  	
//		Encoder_Integral=15000;             //===积分限幅
//	if(Encoder_Integral<-15000)	
//		Encoder_Integral=-15000;              //===积分限幅
//	
//	Velocity = Encoder_last*kp + Encoder_Integral*ki;                          //===速度控制
////	if(Turn_Off(Angle_Balance,Voltage)==1||Flag_Stop==1)   
////		Encoder_Integral=0;      //===电机关闭后清除积分
//	return Velocity;




	
	//encoder = encodernow;//把上一次的值赋给lastencoder
	//读取编码器数据.前进时左负右正，后退时右负左正，一圈是390*4
	encoderL = GetCoder(motoL);
	encoderR = -GetCoder(motoR);
	
	encodernow = encoderL + encoderR;//获取最新速度偏差=测量速度（左右编码器之和）-目标速度（此处为零）
	//一阶低通滤波的算法公式为：Y(n)=αX(n) +(1-α)Y(n-1)
	//式中：α=滤波系数；X(n)=本次采样值；Y(n-1)=上次滤波输出值；Y(n)=本次滤波输出值。
	//一阶低通滤波法采用本次采样值与上次滤波输出值进行加权，得到有效滤波值，使得输出对输入有反馈作用。
	encoder *= 0.7;
	encoder += encodernow * 0.3;
	//encoder = 0.3 * encodernow + (1 - 0.3) * encoder;
	
	pid.integral += encoder;                                       //===积分出位移 积分时间：10ms
	//pid.integral = pid.integral - Movement;                       //===接收遥控器数据，控制前进后退
	if(pid.integral > 15000)  	
		pid.integral = 15000;             //===积分限幅
	if(pid.integral < -15000)	
		pid.integral = -15000;              //===积分限幅
	
	pid.sum = Kp * encoder + Ki * pid.integral;                          //===速度控制
	if(pitch < -40 || pitch > 40 || Voltage < 11/*||1==Flag_Stop*/)//电池电压低于11.1V关闭电机 
	{
		pid.integral = 0;      //===电机关闭后清除积分
	}
	return pid.sum;
}


void PWM_Cntrl(int pwm)
{
	uint16_t minPWM = 400;//就算是平衡点也要有一个微弱的速度
	if(pwm > 0)
		{
			MotoDir(motoL, forward);
			MotoDir(motoR, forward);
			
		}
	else if(pwm < 0)
		{
			MotoDir(motoL, backward);
			MotoDir(motoR, backward);
			pwm = -pwm;
		}
	else if(pwm >= (7200 - minPWM))   
		pwm = 7199 - minPWM;
	else
	{
		MotoDir(motoL, stop);
		MotoDir(motoR, stop);		
	}
	pwm += minPWM;
	if(pitch < -40 || pitch > 40 || Voltage < 11/*||1==Flag_Stop*/)//电池电压低于11.1V关闭电机 
	{
		MotoStop();
	}
	else
	{
		TIM_SetCompare1(TIM1,pwm);
		TIM_SetCompare4(TIM1,pwm);	
	}
}






