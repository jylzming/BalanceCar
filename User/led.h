#ifndef _LED_H_
#define _LED_H_

#include "stm32f10x.h"


typedef enum
{
	ON  = 0,
	OFF
}LED_STATE;

void LED_GPIO_Config(void);
void LED_R(LED_STATE ledstate);
void LED_G(LED_STATE ledstate);
void LED_B(LED_STATE ledstate);
	
#endif
