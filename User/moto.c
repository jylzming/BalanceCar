#include "moto.h"

ENCODER Encoder;

/************************************************************************************
*函数名：Moto_GPIO_Config
*参  数：无
*返回值：无
*描  述：配置马达IO引脚
************************************************************************************/
void Moto_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA, ENABLE);	
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	/* 打开GPIO时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  	
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  	
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	MotoStop();
}

/************************************************************************************
*函数名：MotoPWM
*参  数：无
*返回值：无
*描  述：配置马达PWM，TIM1的通道1和通道4，周期为100uS，需调试修改
************************************************************************************/
void MotoPWM(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	//马达IO口配置和时钟开启
	Moto_GPIO_Config();//这里调用了GPIO配置函数，主函数不用再调用
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);//打开时钟
	
	//马达PWM定时器配置
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;//时钟分频0
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;//向上计数模式
	TIM_TimeBaseInitStructure.TIM_Period = 7200 - 1;//重装载值7199,周期为100uS,10KHz
	TIM_TimeBaseInitStructure.TIM_Prescaler = 0;//计数器的时钟频率(CK_CNT)等于fCK_PSC/( PSC[15:0]+1)
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStructure);//初始化时基单元
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;// PWM模式1，在向上计数时，一旦TIMx_CNT<TIMx_CCR1时通道1为有效电平，否则为无效电平；
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;//空闲状态
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;//输出极性
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;//正向通道有效
	TIM_OCInitStructure.TIM_Pulse = 0;//设置待装入捕获比较寄存器的脉冲值
	
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);//初始化通道1
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable); //CH1预装载使能	
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);//初始化通道4
	TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Enable);//CH4预装载使能	
	
	TIM_ARRPreloadConfig(TIM1, ENABLE);//使能预装载寄存器
	TIM_Cmd(TIM1, ENABLE);//使能TIM1
	TIM_CtrlPWMOutputs(TIM1, ENABLE);//这个要放在TIM_Cmd后面
	
}

/************************************************************************************
*函数名：MotoStop
*参  数：无
*返回值：无
*描  述：配置马达停止
************************************************************************************/
void MotoStop(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
}

/************************************************************************************
*函数名：MotoDir
*参  数：MOTO motor，可取motoL、motoR或motoAB
*参  数：DIR dir， 可取forward、backward或stop，后面再添加左右方向
*返回值：无
*描  述：马达方向控制
************************************************************************************/
void MotoDir(MOTO motor, DIR dir)
{
	switch (motor)
	{
		case motoL:
			switch (dir)
			{
				case forward:
					GPIO_ResetBits(GPIOB, GPIO_Pin_12);
					GPIO_SetBits(GPIOB, GPIO_Pin_13);				
				break;
				case backward:
					GPIO_ResetBits(GPIOB, GPIO_Pin_13);
					GPIO_SetBits(GPIOB, GPIO_Pin_12);					
				break;
				case stop:
					GPIO_ResetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_13);
				break;
			}
		break;
		case motoR:
			switch (dir)
			{
				case forward:
					GPIO_ResetBits(GPIOB, GPIO_Pin_15);
					GPIO_SetBits(GPIOB, GPIO_Pin_14);				
				break;
				case backward:
					GPIO_ResetBits(GPIOB, GPIO_Pin_14);
					GPIO_SetBits(GPIOB, GPIO_Pin_15);					
				break;
				case stop:
					GPIO_ResetBits(GPIOB, GPIO_Pin_14 | GPIO_Pin_15);
				break;
			}			
		break;
		case motoLR:
			switch (dir)
			{
				case forward:
					GPIO_ResetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_15);
					GPIO_SetBits(GPIOB, GPIO_Pin_13 | GPIO_Pin_14);				
				break;
				case backward:
					GPIO_ResetBits(GPIOB, GPIO_Pin_13 | GPIO_Pin_14);
					GPIO_SetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_15);					
				break;
				case stop:
					GPIO_ResetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
				break;
			}			
		break;
		default:
			GPIO_ResetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
		break;
	}
}


/************************************************************************************
*函数名：EncoderConfig
*参  数：无
*返回值：无
*描  述：配置编码器。
		左边马达PB6/TIM4_CH1，PB7/TIM4_CH2；
		右边马达PA0/TIM2_CH1，PA1/TIM2_CH2；
        前进时右正左负，后退时右负左正，一圈是390*4
************************************************************************************/
void EncoderConfig(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
	TIM_ICInitTypeDef TIM_ICInitStructure;  
	
	//打开IO口时钟和定时器时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	
	//PB6 motoL  TIM4_CH1----PB7 motoLB  TIM4_CH2
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	//PA0 motoR  TIM2_CH1----PA1 motoRB  TIM2_CH2
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	//TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, TIM_ICPolarity_BothEdge, TIM_ICPolarity_BothEdge);
	//TIM_EncoderInterfaceConfig(TIM4, TIM_EncoderMode_TI12, TIM_ICPolarity_BothEdge, TIM_ICPolarity_BothEdge);

	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler = 0; // 预分频器 
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF; //设定计数器自动重装值65535
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//选择时钟分频：不分频
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//TIM向上计数,编码器模式应该不起作用了  
	
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

	TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);//使用编码器模式3
	TIM_EncoderInterfaceConfig(TIM4, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);//使用编码器模式3

	TIM_ICStructInit(&TIM_ICInitStructure);
	TIM_ICInitStructure.TIM_ICFilter = 3;
	TIM_ICInit(TIM2, &TIM_ICInitStructure);
	TIM_ICInit(TIM4, &TIM_ICInitStructure);

	TIM_ClearFlag(TIM2, TIM_FLAG_Update);//清除TIM的更新标志位
	TIM_ClearFlag(TIM4, TIM_FLAG_Update);//清除TIM的更新标志位
//	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
//	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	TIM_SetCounter(TIM2,0);
	TIM_SetCounter(TIM4,0);	
	TIM_Cmd(TIM2, ENABLE);
	TIM_Cmd(TIM4, ENABLE);
	
//	TIM2->CNT = 0x7FFF;
//	TIM4->CNT = 0x7FFF;
}

/**************************************************************************
函数名：GetCoder
参  数：定时器
返回值：定时器计数值，一圈390个脉冲*4倍.前进时右正左负，后退时右负左正，一圈是390*4
**************************************************************************/
int GetCoder(MOTO motor)
{
	int Encoder_TIM;    
	switch(motor)
	{
		case motoR:  
			Encoder_TIM= (short)TIM2 -> CNT;  
			TIM2 -> CNT=0;
		break;
		case motoL:  
			Encoder_TIM= (short)TIM4 -> CNT;  
			TIM4 -> CNT=0;
		break;	
		default:  
			Encoder_TIM=0;
		break;
	}
	return Encoder_TIM;	
}


