#ifndef _VOLTAGE_H_
#define _VOLTAGE_H_

#include "stm32f10x.h"

void ADC_GPIO_Config(void);
uint16_t GetADC(void);
float VoltageConvert(void);

#endif


