/**********************************************************************************
 * 文件名  ：uart.c
 * 描述    ：配置串口1，重写print等函数的映射。注意须勾选MDK的 Use Micro LIB        
 * 库版本  ：ST3.5.0
 * 作者    ：牧梦苍穹
 * 时间	 ：2016-07-07
 * 博客    ：http://jylzming.blog.163.com/
**********************************************************************************/
#include "uart.h"

char recv_data[255];
uint8_t recv_cnt = 0;
uint8_t byte_cnt = 0;

/************************************************************************************
*函数名：UART1_Config
*参  数：无
*返回值：无
*描  述：配置串口1波特率为115200，8个数据位，1个停止位，无校验
************************************************************************************/
void UART1_Config(uint32_t baud)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	//使能串口时钟和IO口时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	
	
	//配置IO口，RX悬浮输入，TX推挽复用输出
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	//配置串口，波特率为115200，8个数据位，1个停止位，无校验
	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStructure);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//注意只能分开使能
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);//注意只能分开使能
	USART_Cmd(USART1, ENABLE);
	
	//配置串口中断
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


/************************************************************************************
*函数名：UART3_Config
*参  数：无
*返回值：无
*描  述：配置串口3波特率为9600，8个数据位，1个停止位，无校验
************************************************************************************/
void UART3_Config(uint32_t baud)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	//使能串口时钟和IO口时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE);
	
	//配置IO口，RX悬浮输入，TX推挽复用输出
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//TX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;//RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	//配置串口中断
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	
	
	//配置串口，波特率为9600，8个数据位，1个停止位，无校验
	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART3, &USART_InitStructure);
	
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);//注意只能分开使能
//	USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);//注意只能分开使能
	USART_Cmd(USART3, ENABLE);	
}



/************************************************************************************
*函数名：USART_SendStr
*参  数：USARTx，串口选择，可选USART1~3， str，字符串的地址
*返回值：无
*描  述：发送字符串
************************************************************************************/
void USART_SendStr(USART_TypeDef* USARTx, char *str)
{
	while(*str != '\0')
	{
		USART_SendData(USARTx, (uint16_t)*str);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
		str++;
	}
}



/// 重定向c库函数printf到USART1，注意须勾选MDK的 Use Micro LIB        
int fputc(int ch, FILE *f)
{
		/* 发送一个字节数据到USART1 */
		USART_SendData(USART1, (uint8_t) ch);
		
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);		
	
		return (ch);
}

/// 重定向c库函数scanf到USART1，注意须勾选MDK的 Use Micro LIB        
int fgetc(FILE *f)
{
		/* 等待串口1输入数据 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);

		return (int)USART_ReceiveData(USART1);
}



/************************************************************************************
*函数名：USART1_IRQHandler
*参  数：无
*返回值：无
*描  述：串口1中断服务函数，用于上位机（电脑）通信
************************************************************************************/
void USART1_IRQHandler(void)
{
	uint8_t clear; 
	//如果是接收到一个字节的中断，将其从串口3转发给GPRS模块
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		USART_SendData(USART3, USART_ReceiveData(USART1));//将数据从串口2转发给蓝牙模块
		while (USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);	
		
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);		
	}
	
	//如果是监测到总线空闲的中断，说明数据传输完毕。将计数（数组下标）清零、
	//串口接收到时间修改的标志置位，最后记得要清空中断标志，先读SR、再读DR清除
	if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
	{
		/*add your code here*/
		clear = USART1->SR;
		clear = USART1->DR;
	}
}



/************************************************************************************
*函数名：USART3_IRQHandler
*参  数：无
*返回值：无
*描  述：串口3中断服务函数，用于蓝牙模块通信
************************************************************************************/
void USART3_IRQHandler(void)
{
	char temp = 0; 
	//如果是接收到一个字节的中断，将其保存到数组中
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
		temp = USART_ReceiveData(USART3);
		
		if(byte_cnt == 0 && temp == '\0')//recv_data[0] 不能等于0，否则它是字符串结束符
		{
			//do nothing
		}
		else if(temp == '\0')//接收到0（字符串结束符不保存）
		{
			//do nothing
		}
		else
		{
			recv_data[byte_cnt++] = temp;//每次接收完一帧数据后数组下标都已清零，因此这里直接使用
		}
		
		if(byte_cnt >= 255)
			byte_cnt = 0;
		
		//调试用，将串口3接收到的数据从串口1转发给电脑
		USART_SendData(USART1, USART_ReceiveData(USART3));
		USART_ClearITPendingBit(USART3, USART_IT_RXNE);//清除中断标志
	}
}

