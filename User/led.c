/**********************************************************************************
 * 文件名  ：led.c
 * 描述    ：此文件配置和使用LED，LED1、2、3分别表示红、绿、蓝色LED         
 * 库版本  ：ST3.5.0
 * 作者    ：牧梦苍穹  
 * 博客    ：http://jylzming.blog.163.com/
**********************************************************************************/

#include "led.h"



/************************************************************************************
*函数名：LED_GPIO_Config
*参  数：无
*返回值：无
*描  述：配置LED引脚
************************************************************************************/
void LED_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_Init(GPIOC,&GPIO_InitStructure);
	LED_R(OFF);LED_G(OFF);LED_B(OFF);
}

/************************************************************************************
*函数名：LED_R
*参  数：LED_STATE ledstate，可取ON或者OFF
*返回值：无
*描  述：点亮或者熄灭LED_R，红色
************************************************************************************/
void LED_R(LED_STATE ledstate)
{
	switch (ledstate)
	{
		case ON:
			GPIO_ResetBits(GPIOC, GPIO_Pin_3);
			break;
		case OFF:
			GPIO_SetBits(GPIOC, GPIO_Pin_3);
			break;
	}
}

/************************************************************************************
*函数名：LED_G
*参  数：LED_STATE ledstate，可取ON或者OFF
*返回值：无
*描  述：点亮或者熄灭LED_G，绿色
************************************************************************************/
void LED_G(LED_STATE ledstate)
{
	switch (ledstate)
	{
		case ON:
			GPIO_ResetBits(GPIOC, GPIO_Pin_2);
			break;
		case OFF:
			GPIO_SetBits(GPIOC, GPIO_Pin_2);
			break;
	}
}

/************************************************************************************
*函数名：LED_B
*参  数：LED_STATE ledstate，可取ON或者OFF
*返回值：无
*描  述：点亮或者熄灭LED_B，蓝色
************************************************************************************/
void LED_B(LED_STATE ledstate)
{
	switch (ledstate)
	{
		case ON:
			GPIO_ResetBits(GPIOC, GPIO_Pin_1);
			break;
		case OFF:
			GPIO_SetBits(GPIOC, GPIO_Pin_1);
			break;
	}
}





