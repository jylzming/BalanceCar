#include "encoder.h"







void TIM4_L_encoder_Config(void)
{
  GPIO_InitTypeDef GPIO_InitConfig;
	/*开时钟*/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO,ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
  /*配置GPIO*/
  GPIO_InitConfig.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitConfig.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitConfig.GPIO_Mode  = GPIO_Mode_IPU;
  GPIO_Init(GPIOB,&GPIO_InitConfig);
  /*在TI1与TI2的上升沿与下降沿都计数，一个脉冲计数4次*/
  TIM_EncoderInterfaceConfig(TIM4, 
                             TIM_EncoderMode_TI12,
                             TIM_ICPolarity_BothEdge,
                             TIM_ICPolarity_BothEdge);
	/*使能TIM3*/
  TIM_Cmd(TIM4,ENABLE);
	TIM4->CNT=0x7fff;
}

void TIM2_R_encoder_Config(void)
{
  GPIO_InitTypeDef GPIO_InitConfig;
	/*开时钟*/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO,ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
  /*配置GPIO*/
  GPIO_InitConfig.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1;
  GPIO_InitConfig.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitConfig.GPIO_Mode  = GPIO_Mode_IPU;
  GPIO_Init(GPIOA,&GPIO_InitConfig);
  /*在TI1与TI2的上升沿与下降沿都计数，一个脉冲计数4次*/
  TIM_EncoderInterfaceConfig(TIM2, 
                             TIM_EncoderMode_TI12,
                             TIM_ICPolarity_BothEdge,
                             TIM_ICPolarity_BothEdge);
	/*使能TIM3*/
  TIM_Cmd(TIM2,ENABLE);
	TIM2->CNT=0x7fff;
}
