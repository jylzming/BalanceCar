#ifndef _MOTO_H_
#define _MOTO_H_

#include "stm32f10x.h"

typedef enum
{
	 motoL = 0x01,
	 motoR = 0x02,
	 motoLR = 0x03
}MOTO;

typedef enum
{
	forward = 1,
	backward = -1,
	stop = 0
}DIR;

typedef struct 
{
	DIR dir;
	uint16_t code;
}ENCODER;

extern ENCODER Encoder;

void Moto_GPIO_Config(void);
void MotoPWM(void);
void MotoStop(void);
void MotoDir(MOTO motor, DIR dir);
void EncoderConfig(void);
int GetCoder(MOTO motor);

void MOTOR_PWM_Config(void);

#endif
