#include "key.h"
#include "led.h"
#include "systick.h"
#include "uart.h"

void Key_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	
	//KEY1----PA12
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	//KEY2----PC12
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_Init(GPIOC,&GPIO_InitStructure);	

	//KEY3----PB5
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_Init(GPIOB,&GPIO_InitStructure);	
}

KEY KeyScan(void)
{
	uint8_t i = 0;
	while(1)//这里记得要根据情况修改，否则调用keyscan会一直检测到有按键才能跳出
	{	
		
		if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12))
		{
			while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12))
			{
				i++;
				Delay_ms(100);
				if(i >= 15)
				{
					i = 0;
					return key1_press;
				}
				else
				{
					//do nothing
				}			
			}
			return key1;	
		}
		if(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12))
		{
			while(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12))
			{
				i++;
				Delay_ms(100);
				if(i >= 15)
				{
					i = 0;
					return key2_press;
				}
				else
				{
					//do nothing
				}
			}
			return key2;			
		}
		if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_5))
		{
			while(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_5))
			{
				i++;
				Delay_ms(100);
				if(i >= 15)
				{
					i = 0;
					return key3_press;
				}
				else
				{
					//do nothing
				}
			}
			return key3;
		}
		else
		{
			//do nothing
		}
		return key_notclick;
	}
}
