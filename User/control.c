#include "control.h"
#include "math.h"
#include "moto.h"
#include "gyro.h"
#include "voltage.h"

extern int Balance_PWM,Speed_PWM,Turn_PWM;
extern int MotoL_PWM,MotoR_PWM;
u8 Flag_Target;
extern int encoderL,encoderR; //左右编码器的脉冲计数
extern short Temperature;//温度
extern int Voltage;//电池电压
extern float pitch;//俯仰角

extern short Accel[3];//加速度原始数据
extern short Gyro[3];//陀螺仪原始数据
extern float AccConvert[3];//加速度转换数据
extern float GyrConvert[3];//陀螺仪转换数据

extern int qianhou_flag;//前后标志，1向前，-1向后
extern int turn_flag;//转向标志，-1向左，1向右
//float Gyro_Balance,Gyro[2]; //陀螺仪 转向陀螺仪
float Acceleration_Z;                      //Z轴加速度计  
extern float pitch;//俯仰角
u8 Flag_Stop=1;


void Exchang_Data(short *accel,short *gyro,float *accconvert,float *gyroconvert)
{
	uint8_t i;
	for(i=0;i<3;i++)
	{
		accconvert[i] = accel[i] / 16384.0f;//a = 2g*(ACC/32768)其中g为重力加速度，ACC为陀螺仪获取的数据
		gyroconvert[i] = gyro[i] / 16.384f;//MPU6050数据是16位的数，满量程-+2000，即4000. 65536/4000=16.384
	}
}


/**************************************************************************
**************************************************************************/
int Balance(float angle,float Gyro)
{
	//Balance_PWM  = Balance(pitch,(-GyrConvert[1])); 
	float error, kp=200, kd=0.60;
	int balance;
	error = angle - ZERO_PIONT; //ZERO_PIONT平衡的角度值 
	balance = kp * error + Gyro * kd;   
	return balance;
}

/**************************************************************************
**************************************************************************/
int Speed(int encoder_left,int encoder_right)
{
	static float speed,encodernow,encoder,Movement;
	static float integral;
	
	float kp = 30,ki = kp / 200;
	
	//读取编码器的值
	encoderL = GetCoder(motoL);      
	encoderR = -GetCoder(motoR);    
	
	encodernow = encoderL+encoderR;                    
	encoder = 0.3 * encodernow + (1 - 0.3) * encoder;//互不滤波
	
	integral += encoder; 
	//integral = integral - Movement;                       //===接收遥控器数据，控制前进后退
	
	//积分限幅
	if(integral>15000)  	
		integral=15000;     
	if(integral<-15000)	
		integral=-15000;   
	
	//PID计算
	speed = kp * encoder + ki * integral; 
	
	if(Turn_Off(pitch,Voltage) == 1)   
		integral=0;      //===电机关闭后清除积分
	
	return speed;
}

/**************************************************************************
函数功能：转向控制  修改转向速度，请修改Turn_Amplitude即可
入口参数：左轮编码器、右轮编码器、Z轴陀螺仪
返回  值：转向控制PWM
**************************************************************************/
int turn(int encoder_left,int encoder_right,float gyro)//转向控制
{
	static float Turn_Target,Turn,Encoder_temp;
	static float Turn_Convert=0.7,Turn_Count,Kp=42,Kd=0;
	float Turn_Amplitude=50;

	if(turn_flag != 0)                      //这一部分主要是根据旋转前的速度调整速度的起始速度，增加小车的适应性
	{
		if(++Turn_Count==1)
			Encoder_temp=myabs(encoder_left+encoder_right);
		
		Turn_Convert = 50 / Encoder_temp;
		
		if(Turn_Convert < 0.4)
			Turn_Convert=0.4;
		if(Turn_Convert > 1)
			Turn_Convert=1;
	}
	else
	{
		Turn_Convert=0.7;
		Turn_Count=0;
		Encoder_temp=0;
	}
	
	if(turn_flag == -1)	 //向左          
		Turn_Target -= Turn_Convert;        //===接收转向遥控数据
	else if(turn_flag == 1)	//向右     
		Turn_Target += Turn_Convert;        //===接收转向遥控数据
	else 
		Turn_Target=0;  
	
	//转向速度限幅
	if(Turn_Target > Turn_Amplitude)  
		Turn_Target = Turn_Amplitude;    
	if(Turn_Target < -Turn_Amplitude) 
		Turn_Target = -Turn_Amplitude;   //===转向速度限幅
	
	//前进后退
	if(qianhou_flag != 0)  
		Kd = 0.6;                         //===接收转向遥控数据直立行走的时候增加陀螺仪就纠正
	else 
		Kd = 0;
	//=============转向PD控制器=======================//
	Turn = Turn_Target * Kp + gyro * Kd;                 //===结合Z轴陀螺仪进行PD控制
	return Turn;
}
/**************************************************************************
函数名：Set_Pwm
参  数：moto1左轮PWM、moto2右轮PWM
返回值：无
描  述：赋值给PWM寄存器
**************************************************************************/
void Set_Pwm(int moto1,int moto2)
{
	uint16_t minPWM = 500;//就算是平衡点也要有一个微弱的速度
	//left
	if(MotoL_PWM > 0)//向前
		{
			MotoDir(motoL, forward);
		}
	else if(MotoL_PWM < 0)//向后
		{
			MotoDir(motoL, backward);
			MotoL_PWM = -MotoL_PWM;
		}
	else if(MotoL_PWM >= (7200 - minPWM))//PWM宽度大于100% 
		MotoL_PWM = 7199 - minPWM;
	else//未知情况
	{
		MotoDir(motoL, stop);
	}
	MotoL_PWM += minPWM;
	
	//right
	if(MotoR_PWM > 0)//向前
		{
			MotoDir(motoR, forward);
			
		}
	else if(MotoR_PWM < 0)//向后
		{
			MotoDir(motoR, backward);
			MotoR_PWM = -MotoR_PWM;
		}
	else if(MotoR_PWM >= (7200 - minPWM)) //PWM宽度大于100%   
		MotoR_PWM = 7199 - minPWM;
	else//未知情况
	{
		MotoDir(motoR, stop);		
	}
	
	//最终PWM脉冲要加上最小值，要不然肯定跌
	MotoR_PWM += minPWM;
	
	//俯仰角大于一定值和电压小于一定值让电机停止转动
	if(pitch < -30 || pitch > 30 || Voltage < 11/*||1==Flag_Stop*/)
	{
		MotoStop();
	}
	else
	{
		TIM_SetCompare1(TIM1,MotoL_PWM);
		TIM_SetCompare4(TIM1,MotoR_PWM);	
	}
}


/**************************************************************************
函数功能：异常关闭电机
入口参数：倾角和电压
返回  值：1：异常  0：正常
**************************************************************************/
u8 Turn_Off(float angle, int voltage)
{
	u8 temp;
	if(angle<-40||angle>40||voltage<11)//电池电压低于11.1V关闭电机
	{	                                                 //===倾角大于40度关闭电机
		temp=1;                                            //===Flag_Stop置1关闭电机
		MotoStop();
	}
	else
	temp=0;
	return temp;
}

void Yijielvbo(float angle_m, float gyro_m)
{
   pitch = 0.02 * angle_m+ (1-0.02) * (pitch + gyro_m * 0.005);
}

/**************************************************************************
**************************************************************************/
void Get_Angle(u8 way)
{
	//float Accel[1],Accel[0],Accel[2],Gyro[1],Gyro[2];
	float tmp1, tmp2;
	//读取陀螺仪原始数据
	MPU6050ReadAcc(Accel);	
	MPU6050ReadGyro(Gyro);		
	//读取陀螺仪温度
	MPU6050ReadTemp(&Temperature);   
	Exchang_Data(Accel, Gyro, AccConvert, GyrConvert);

	//Gyro_Balance = -Gyro[1];                                  //更新平衡角速度
	
	tmp1 = atan2(Accel[0], Accel[2]) * 180 / PI;                 //计算倾角
	
//	tmp2 = Gyro[1]/16.384;                                    //陀螺仪量程转换
//	Yijielvbo(tmp1, -tmp2);    //互补滤波
	pitch = 0.02 * tmp1 + (1-0.02) * (pitch + (-GyrConvert[1]) * 0.005);
//	Gyro[2] = GyrConvert[2];//Gyro[2]??//更新转向角速度
//	Acceleration_Z=AccConvert[2];                                //===更新Z轴加速度计
}
/**************************************************************************
函数功能：绝对值函数
入口参数：int
返回  值：unsigned int
**************************************************************************/
int myabs(int a)
{
	int temp;
	if(a<0)  temp=-a;
	else temp=a;
	return temp;
}
/**************************************************************************
函数功能：检测小车是否被拿起
入口参数：int
返回  值：unsigned int
**************************************************************************/
int Pick_Up(float Acceleration,float Angle,int encoder_left,int encoder_right)
{
	static u16 flag,count0,count1,count2;
	if(flag==0)                                                                   //第一步
	{
		if(myabs(encoder_left)+myabs(encoder_right)<30)                         //条件1，小车接近静止
		count0++;
		else
		count0=0;
		if(count0>10)
		flag=1,count0=0;
	}
	if(flag==1)                                                                  //进入第二步
	{
		if(++count1>200)       count1=0,flag=0;                                 //超时不再等待2000ms
		if(Acceleration>26000&&(Angle>(-20+ZERO_PIONT))&&(Angle<(20+ZERO_PIONT)))   //条件2，小车是在0度附近被拿起
		flag=2;
	}
	if(flag==2)                                                                  //第三步
	{
		if(++count2>100)       count2=0,flag=0;                                   //超时不再等待1000ms
		if(myabs(encoder_left+encoder_right)>200)                                 //条件3，小车的轮胎因为正反馈达到最大的转速
		{
			flag=0;
			return 1;                                                               //检测到小车被拿起
		}
	}
	return 0;
}
/**************************************************************************
函数功能：检测小车是否被放下
入口参数：int
返回  值：unsigned int
**************************************************************************/
int Put_Down(float Angle,int encoder_left,int encoder_right)
{
	static u16 flag,count;
	if(Flag_Stop==0)                           //防止误检
	return 0;
	if(flag==0)
	{
		if(Angle>(-10+ZERO_PIONT)&&Angle<(10+ZERO_PIONT)&&encoder_left==0&&encoder_right==0)         //条件1，小车是在0度附近的
		flag=1;
	}
	if(flag==1)
	{
		if(++count>50)                                          //超时不再等待 500ms
		{
			count=0;flag=0;
		}
		if(encoder_left<-3&&encoder_right<-3&&encoder_left>-30&&encoder_right>-30)                //条件2，小车的轮胎在未上电的时候被人为转动
		{
			flag=0;
			flag=0;
			return 1;                                             //检测到小车被放下
		}
	}
	return 0;
}
