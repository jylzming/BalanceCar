/**
******************************************************************************
* @file    PID_Control.c
* @author  willieon
* @version V0.1
* @date    January-2015
* @brief   PID控制算法函数代码
*
*
******************************************************************************
**/

#include "PID_Control.h"
#include "math.h"

/*************************************************************************************
*        名    称： double PIDCalc( PID *pp, double NextPoint ,double SepLimit)
*        功    能： PID控制运算
*        入口参数： PID *pp  - 定义的运算所需变量的结构体
*                  NextPoint - 负反馈输入值
*                  SepLimit  - 积分分离上限
*        出口参数： 返回PID控制量
*        说    明： 默认不进行积分分离，如果用户需要使用积分分离，需在PID_Control.h中
*                                将 #define IF_THE_INTEGRAL_SEPARATION  0  改为
*                            #define IF_THE_INTEGRAL_SEPARATION  1
*        调用方法： 进行积分分离时入口参数为3个，具体方法如下：
*                                PID PIDControlStruct ;   //定义PID运算结构体
*                                PIDInit(50, 0.24, 0.04, 0.2, &PIDControlStruct);//结构体初始化，注意&符号不能省
*                                ControlData = PIDCalc(ReadData, 200, &PIDControlStruct);   //控制量 = PIDCalc(反馈值,积分分离上限,PID运算结构体)
*
***************************************************************************************
*/

#if IF_THE_INTEGRAL_SEPARATION

double PIDCalc(double NextPoint ,double SepLimit, PID *pp)
{
    double dError, Error,Flag;
    Error = pp->SetPoint - NextPoint;         // 偏差
    if(abs(Error) > SepLimit)        //当偏差大于分离上限积分分离
    {
        Flag = 0;
    }
    else       //当偏差小于分离上限，积分项不分离
    {
        Flag = 1;
        pp->SumError += Error;         // 积分
    }
    dError = pp->LastError - pp->PrevError;         // 当前微分
    pp->PrevError = pp->LastError;
    pp->LastError = Error;
    return
    (
        pp->Proportion * Error + Flag * pp->Integral * pp->SumError + pp->Derivative * dError
    );
}

#else

double PIDCalc( double NextPoint, PID *pp)
{
    double dError, Error;
    Error = pp->SetPoint - NextPoint;                         // 偏差
    pp->SumError += Error;                                        // 积分
    dError = pp->LastError - pp->PrevError;                // 当前微分
    pp->PrevError = pp->LastError;
    pp->LastError = Error;
    return (pp->Proportion * Error + pp->Integral * pp->SumError + pp->Derivative * dError   // 比例项 + 积分项 + 微分项
    );
}

#endif


/*************************************************************************************
*        名    称： double PIDCalc( PID *pp, double NextPoint ,double SepLimit)
*        功    能： PID初始化设定
*        入口参数： PID *pp  - 定义的运算所需变量的结构体
*                           SetPoint - 设定的目标值
*                           Proportion，Integral ，Derivative - P,I,D系数
*        出口参数： 无
*        说    明：
*        调用方法：  PID PIDControlStruct ;   //定义PID运算结构体
*                                PIDInit(50, 0.24, 0.04, 0.2, &PIDControlStruct);//结构体初始化，注意&符号不能省
*                                因为函数需要传入一个指针，需要对结构体取首地址传给指针
*
***************************************************************************************
*/


void PIDInit (double SetPoint, double Proportion, double Integral, double Derivative, PID *pp)
{
    pp -> SetPoint = SetPoint; // 设定目标 Desired Value
    pp -> Proportion = Proportion; // 比例常数 Proportional Const
    pp -> Integral = Integral; // 积分常数 Integral Const
    pp -> Derivative = Derivative; // 微分常数 Derivative Const
    pp -> LastError = 0; // Error[-1]
    pp -> PrevError = 0; // Error[-2]
    pp -> SumError = 0; // Sums of Errors

    //memset ( pp,0,sizeof(struct PID));   //need include "string.h"
}




#include "kalman.h"

#include "stdio.h"
#include "stdlib.h"
#include "PID_Control.h"

void main(void)

{
    KalmanCountData k;
    PID PIDControlStruct;
    Kalman_Filter_Init(&k);
    PIDInit(50, 1, 0.04, 0.2, &PIDControlStruct);
    int m,n;

    double out;

    for(int a = 0;a<80;a++)
    {
        m = 1+ rand() %100;
        n = 1+ rand() %100;
        Kalman_Filter((float)m,(float)n,&k);
        out = PIDCalc(k.Angle_Final, &PIDControlStruct);
        printf("%3d and %3d is %6f -pid- %6f\r\n",m,n,k.Angle_Final,out);

    }
}






#include <reg52.h>
typedef unsigned char      uChar8;
typedef unsigned int       uInt16;
typedef unsigned long int  uInt32;

sbit ConOut = P1^1;     //加热丝接到P1.1口

typedef struct PID_Value
{
    uInt32 chazhiVal[3];          //差值保存，给定和反馈的差值
    uChar8 fuhaoFlag[3];          //符号，1则对应的为负数，0为对应的为正数
    uChar8 KP;             //比例系数
    uChar8 KI;             //积分常数
    uChar8 KD;             //微分常数
    uInt16 lastVal;             //上一时刻值
    uInt16 setVal;             //设定值
    uInt16 realVal;             //实际值
}PID_ValueStr;

PID_ValueStr PID;               //定义一个结构体，这个结构体用来存算法中要用到的各种数据
bit g_bPIDRunFlag = 0;          //PID运行标志位，PID算法不是一直在运算。而是每隔一定时间，算一次。
/* ********************************************************
/* 函数名称：PID_Operation()
/* 函数功能：PID运算
/* 入口参数：无（隐形输入，系数、设定值等）
/* 出口参数：无（隐形输出，U(k)）
/* 函数说明：U(k)=KP*[E(k)-E(k-1)]+KI*E(k)+KD*[E(k)-2E(k-1)+E(k-2)]
******************************************************** */
void PID_Operation(void)
{
    uInt32 Temp[3] = {0};   //中间临时变量
    uInt32 PostSum = 0;     //正数和
    uInt32 NegSum = 0;      //负数和
    if(PID.setVal > PID.realVal)                //设定值大于实际值否？
    {
        if(PID.setVal - PID.realVal > 10)      //偏差大于10否？
            PID.lastVal = 100;                  //偏差大于10为上限幅值输出(全速加热)
        else                                    //否则慢慢来
        {
            Temp[0] = PID.setVal - PID.realVal;    //偏差<=10,计算E(k)
            PID.fuhaoFlag[1] = 0;                     //E(k)为正数,因为设定值大于实际值
            /* 数值进行移位，注意顺序，否则会覆盖掉前面的数值 */
            PID.chazhiVal[2] = PID.chazhiVal[1];
            PID.chazhiVal[1] = PID.chazhiVal[0];
            PID.chazhiVal[0] = Temp[0];
            /* =================================================================== */
            if(PID.chazhiVal[0] > PID.chazhiVal[1])              //E(k)>E(k-1)否？
            {
                Temp[0] = PID.chazhiVal[0] - PID.chazhiVal[1];  //E(k)>E(k-1)
                PID.fuhaoFlag[0] = 0;                         //E(k)-E(k-1)为正数
            }
            else
            {
                Temp[0] = PID.chazhiVal[1] - PID.chazhiVal[0];  //E(k)<E(k-1)
                PID.fuhaoFlag[0] = 1;                         //E(k)-E(k-1)为负数
            }
            /* =================================================================== */
            Temp[2] = PID.chazhiVal[1] * 2;                   //2E(k-1)
            if((PID.chazhiVal[0] + PID.chazhiVal[2]) > Temp[2]) //E(k-2)+E(k)>2E(k-1)否？
            {
                Temp[2] = (PID.chazhiVal[0] + PID.chazhiVal[2]) - Temp[2];
                PID.fuhaoFlag[2]=0;                           //E(k-2)+E(k)-2E(k-1)为正数
            }
            else                                            //E(k-2)+E(k)<2E(k-1)
            {
                Temp[2] = Temp[2] - (PID.chazhiVal[0] + PID.chazhiVal[2]);
                PID.fuhaoFlag[2] = 1;                         //E(k-2)+E(k)-2E(k-1)为负数
            }
            /* =================================================================== */
            Temp[0] = (uInt32)PID.KP * Temp[0];        //KP*[E(k)-E(k-1)]
            Temp[1] = (uInt32)PID.KI * PID.chazhiVal[0]; //KI*E(k)
            Temp[2] = (uInt32)PID.KD * Temp[2];        //KD*[E(k-2)+E(k)-2E(k-1)]
            /* 以下部分代码是讲所有的正数项叠加，负数项叠加 */
            /* ========= 计算KP*[E(k)-E(k-1)]的值 ========= */
            if(PID.fuhaoFlag[0] == 0)
                PostSum += Temp[0];                         //正数和
            else
                NegSum += Temp[0];                          //负数和
            /* ========= 计算KI*E(k)的值 ========= */
            if(PID.fuhaoFlag[1] == 0)
                PostSum += Temp[1];                         //正数和
            else
                ;   /* 空操作。就是因为PID.setVal > PID.realVal（即E(K)>0）才进入if的，
                    那么就没可能为负，所以打个转回去就是了 */
            /* ========= 计算KD*[E(k-2)+E(k)-2E(k-1)]的值 ========= */
            if(PID.fuhaoFlag[2]==0)
                PostSum += Temp[2];             //正数和
            else
                NegSum += Temp[2];              //负数和
            /* ========= 计算U(k) ========= */
            PostSum += (uInt32)PID.lastVal;
            if(PostSum > NegSum)                 //是否控制量为正数
            {
                Temp[0] = PostSum - NegSum;
                if(Temp[0] < 100 )               //小于上限幅值则为计算值输出
                    PID.lastVal = (uInt16)Temp[0];
                else PID.lastVal = 100;         //否则为上限幅值输出
            }
            else                                //控制量输出为负数，则输出0(下限幅值输出)
                PID.lastVal = 0;
        }
    }
    else PID.lastVal = 0;                       //同上，嘿嘿
}
/* ********************************************************
/* 函数名称：PID_Output()
/* 函数功能：PID输出控制
/* 入口参数：无（隐形输入，U(k)）
/* 出口参数：无（控制端）
******************************************************** */
void PID_Output(void)
{
    static uInt16 iTemp;
    static uChar8 uCounter;
    iTemp = PID.lastVal;
    if(iTemp == 0)
        ConOut = 1;     //不加热
    else ConOut = 0;    //加热
    if(g_bPIDRunFlag)   //定时中断为100ms(0.1S)，加热周期10S(100份*0.1S)
    {
        g_bPIDRunFlag = 0;
        if(iTemp) iTemp--;      //只有iTemp>0，才有必要减“1”
        uCounter++;
        if(100 == uCounter)
        {
            PID_Operation();    //每过0.1*100S调用一次PID运算。
            uCounter = 0;
        }
    }
}
/* ********************************************************
/* 函数名称：PID_Output()
/* 函数功能：PID输出控制
/* 入口参数：无（隐形输入，U(k)）
/* 出口参数：无（控制端）
******************************************************** */
void Timer0Init(void)
{
    TMOD |= 0x01;   // 设置定时器0工作在模式1下
    TH0 = 0xDC;
    TL0 = 0x00;     // 赋初始值
    TR0 = 1;        // 开定时器0
    EA = 1;         // 开总中断
    ET0 = 1;        // 开定时器中断
}

void main(void)
{
    Timer0Init();
    while(1)
    {
        PID_Output();
    }
}

void Timer0_ISR(void) interrupt 1
{
    static uInt16 uiCounter = 0;
    TH0 = 0xDC;
    TL0 = 0x00;
    uiCounter++;
    if(100 == uiCounter)
    {
        g_bPIDRunFlag = 1;
    }
}

/*****************************************************************************/
#define	 Kp		0.8
#define  Ki		0.7
#define	 Kd		0.05

struct pid_data
{
	float SetPoint;	//Desired Value
	float FeedBack;	//feedback value
	float err;		//实际值与设定值的差值
	float err_last; //上一次的实际值与设定值的差值
	float integral; //误差之和
	float u_sum;
};
typedef struct pid_data		pid_t;

//pid struct data init
pid_t *pid_init(float SetPoint, float FeedBack, float err, float err_last,float integral, float u_sum)
{
	struct pid_data* tset = malloc(sizeof(struct pid_data));

	tset->SetPoint 	= SetPoint;
	tset->FeedBack 	= FeedBack;
	tset->err 		= err;
	tset->err_last 	= err_last;
	tset->integral 	= integral;
	tset->u_sum		= u_sum;

	return tset;
}

float PidCalc(pid_t *pid)
{
	float err;
    int M = 2;
	int A = 0.4,B = 0.6;
	float flag;

    pid->err = pid->SetPoint - pid->FeedBack;//计算差值
    pid->integral = pid->integral + (pid->err + pid->err_last) / 2;//计算差值之和

    err  = pid->err;
    if (M == 1) {
        if(abs(err) <= B)
        {
            flag = 1.0;
        }
        else if ((abs(err) > B) && (abs(err) <= (A+B)))
        {
            flag = (A - abs(err) + B) / A;
        }
        else
        {
            flag = 0;
        }
    }
    else if(M == 2)
    {
        flag = 1.0;
    }

    //Uo = Kp*本次误差 + Ki*所有误差之和 + Kd*误差变化率
    pid->u_sum = Kp*pid->err + flag*Ki*pid->integral + Kd*(pid->err - pid->err_last);
    pid->FeedBack = pid->u_sum*1.0;
	pid->err_last = pid->err;

    return pid->FeedBack;
}
